import {
    deleteCheckItem,
    createCheckItem,
    updateCheckItem,
    getCheckItems,
} from "./apiRequests"

export const handleDeleteCheckItems = (
    checkItemId,
    checkList,
    updateCheckList,
    setError
) => {
    deleteCheckItem(checkList.id, checkItemId)
        .then((result) => {
            const filteredCheckItems = checkList.checkItems.filter((checkItem) => checkItem.id !== checkItemId)
            updateCheckList(checkList.id, filteredCheckItems)
        })
        .catch((err) => setError(true))
}

export const handleCreateCheckItems = (
    checkList,
    checkItemName,
    updateCheckList,
    setError
) => {
    createCheckItem(checkList.id, checkItemName)
        .then((result) => {
            const newCheckItems = [...checkList.checkItems, result]
            updateCheckList(checkList.id, newCheckItems)
        })
        .catch((err) => setError(true))
}

export const handleCheckItemState = (
    checkList,
    checkItemId,
    value,
    updateCheckList,
    setError
) => {
    updateCheckItem(checkList.idCard, checkItemId, value ? "complete" : "incomplete")
        .then((result) => {
            const updatedCheckItem = checkList.checkItems.map((checkItem) => {
                return checkItem.id === checkItemId ? {...checkItem, state:result.state} : checkItem
            })
            updateCheckList(checkList.id, updatedCheckItem)
        })
        .catch((err) => setError(true))
}
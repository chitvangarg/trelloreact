import { createCheckList, deleteCheckList, getCheckList } from "../apiRequests"

export const handleChecklistCreation = (
    checkListName,
    setCheckLists,
    setCheckListName,
    cardId,
    setLoading,
    setError
) => {
    if (checkListName !== "") {
        createCheckList(cardId, checkListName)
            .then((checkList) => {
                setLoading(false)
                setCheckLists((prevCheckList) => [
                    ...prevCheckList,
                    checkList,
                ])
                setCheckListName("")
            })
            .catch((err) => {
                setError(true)
            })
    }
}

export const handleCheckListDeletion = (
    checkListId,
    setCheckLists,
    setError
) => {
    deleteCheckList(checkListId)
        .then((deletedCheckList) => {
            setCheckLists((checkLists) =>
                checkLists.filter((checkList) => checkList.id !== checkListId)
            )
        })
        .catch((err) => setError(true))
}

export const getAllCheckList = (
    cardId,
    setCheckLists,
    setError,
    setLoading
) => {
    getCheckList(cardId)
        .then((returnedCheckLists) => {
            console.log(returnedCheckLists)
            setCheckLists([...returnedCheckLists])
            setLoading(false)
        })
        .catch((err) => setError(true))
}

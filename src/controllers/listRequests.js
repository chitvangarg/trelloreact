import { addNewList, deleteList, getLists } from "../apiRequests"

export const handleListCreation = (
    setLists,
    setListName,
    listName,
    boardId,
    setError
) => {
    addNewList(listName, boardId)
        .then((list) => {
            console.log(list.id)
            setLists((lists) => [...lists, list])
            setListName("")
        })
        .catch((err) => setError(true))
}

export const handleListDelete = (listId, setLists, setError) => {
    deleteList(listId)
        .then((listItem) =>
            setLists((lists) => lists.filter((list) => list.id !== listItem.id))
        )
        .catch((err) => setError(true))
}

export const getAllLists = (boardId, setLists, setLoading, setError) => {
    getLists(boardId)
        .then((list) => {
            setLists(list)
            setLoading(false)
        })
        .catch((err) => setError(true))
}

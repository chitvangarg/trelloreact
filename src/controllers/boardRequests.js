import { addNewBoard, getBoards } from "../apiRequests"

export const addBoards = (boardName, setBoards, setBoardName, setOpen, setError) => {
    if (boardName !== "") {
        addNewBoard(boardName)
            .then((res) => setBoards((boards) => [...boards, res]))
            .catch((err) => console.log(err.message))

        setOpen(false)
        setBoardName("")
        setError(false)
    }
}

export const getAllBoards = (setBoards, setLoading, setError) => {
    getBoards()
        .then((boards) => {
            setBoards([...boards])
            setLoading(false)
        })
        .catch((err) => {
            setError(true)
        })
}

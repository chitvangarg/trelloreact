import React, {useState } from "react"
import { Box } from "@chakra-ui/react"

import AddCheckItem from "./AddCheckItem"

import {
    handleCreateCheckItems,
    handleDeleteCheckItems,
    handleCheckItemState,
} from "../../controllers/checkItemRequests"
import CheckItems from "./CheckItems"
import ProgressBar from "../ProgressBar"
import Error from "../Error"

const CheckItemsContainer = ({
    checkList,
    checkItemInput,
    setCheckItemInput,
    updateCheckList,
}) => {
    const [checkItemName, setCheckItemName] = useState("")
    const [isError, setError] = useState(false)

    const addCheckItems = () => {
        handleCreateCheckItems(
            checkList,
            checkItemName,
            updateCheckList,
            setError
        )
    }

    const deleteCheckItems = (checkItemId) => {
        handleDeleteCheckItems(
            checkItemId,
            checkList,
            updateCheckList,
            setError
        )
    }

    const updateCheckItemState = (checkItemId, value) => {
        handleCheckItemState(
            checkList,
            checkItemId,
            value,
            updateCheckList,
            setError
        )
    }

    const checkedItemsCount = checkList.checkItems.reduce(
        (total, checkItem) => {
            return checkItem.state === "complete" ? (total += 1) : total
        },
        0
    )

    if (isError) {
        return <Error error="Unable to make request" />
    } else {
        return (
            <Box>
                <ProgressBar
                    value={Math.floor(
                        (checkedItemsCount / checkList.checkItems.length) * 100
                    )}
                />
                <CheckItems
                    deleteCheckItem={deleteCheckItems}
                    checkItems={checkList.checkItems}
                    handleCheckItemState={updateCheckItemState}
                />

                {checkItemInput && (
                    <AddCheckItem
                        setCheckItemInput={setCheckItemInput}
                        setCheckitemName={setCheckItemName}
                        addCheckitems={addCheckItems}
                        checkItemName={checkItemName}
                    />
                )}
            </Box>
        )
    }
}

export default CheckItemsContainer

import React from "react"
import { Checkbox, Flex } from "@chakra-ui/react"
import { DeleteIcon } from "@chakra-ui/icons"

const CheckItems = ({
    checkItems,
    deleteCheckItem,
    handleCheckItemState,
}) => {
    const deleteIconStyle = {
        ":hover": {
            cursor: "pointer",
            color: "#ff0000",
        },
    }

    return (
        <>
            {checkItems &&
                checkItems.map((checkItem) => {
                    return (
                        <Flex
                            w="100%"
                            bg="#fff"
                            px="1rem"
                            py="0.5rem"
                            justifyContent="space-between"
                            key={checkItem.id}
                        >
                            <Checkbox
                                type="checkbox"
                                isChecked={
                                    checkItem.state === "complete"
                                        ? true
                                        : false
                                }
                                onChange={(e) => {
                                    handleCheckItemState(
                                        checkItem.id,
                                        e.target.checked
                                    )
                                }}
                            >
                                {checkItem.name}
                            </Checkbox>
                            <DeleteIcon
                                sx={deleteIconStyle}
                                onClick={() => {
                                    deleteCheckItem(checkItem.id)
                                }}
                            />
                        </Flex>
                    )
                })}
        </>
    )
}

export default CheckItems

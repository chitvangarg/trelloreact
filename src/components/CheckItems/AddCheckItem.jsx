import React from "react"
import { Flex, Button, Box } from "@chakra-ui/react"
import InputNames from "../InputNames"
import EmptyInputValidation from "../EmptyInputValidation"

const AddCheckItem = ({
    setCheckItemInput,
    setCheckitemName,
    addCheckitems,
    checkItemName,
}) => {
    return (
        <Flex justifyContent="space-around" alignItems="center" w="100%">
            <Box>
                <InputNames
                    valueHandler={setCheckitemName}
                    displayer={setCheckItemInput}
                />
            </Box>
            <Box>
                <EmptyInputValidation
                    section="Checkitem"
                    displayer={setCheckItemInput}
                    value={checkItemName}
                    handler={addCheckitems}
                />
            </Box>
        </Flex>
    )
}

export default AddCheckItem

import React from "react"
import { Progress } from "@chakra-ui/react"

const ProgressBar = ({ value }) => {
    return (
        <>
            <Progress value={isNaN(value) ? 0 : value}></Progress>
        </>
    )
}

export default ProgressBar

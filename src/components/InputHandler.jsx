import React from "react"
import EmptyInputValidation from "./EmptyInputValidation"
import InputNames from "./InputNames"
import { AddIcon } from "@chakra-ui/icons"
import { Box, Button, Flex, Text } from "@chakra-ui/react"
import { useState } from "react"

const InputHandler = ({
    section,
    inputValue,
    inputValueHandler,
    actionFunction,
    id = undefined,
}) => {
    const [show, setShow] = useState(false)

    return (
        <>
            {show ? (
                <Flex alignItems="center"  columnGap="20px"> 
                    <Box>
                        <InputNames
                            valueHandler={inputValueHandler}
                            displayer={setShow}
                        />
                    </Box>
                    <Box>
                        <EmptyInputValidation
                            section={section}
                            displayer={setShow}
                            handler={actionFunction}
                            value={inputValue}
                            id={id}
                        />
                    </Box>
                </Flex>
            ) : (
                <Button
                    onClick={() => setShow(true)}
                    minW="200px"
                    bgColor="#3498DB"
                    color="#fff"
                    my="1rem"
                >
                    <AddIcon />
                    <Text px="10px">Add new {section}</Text>
                </Button>
            )}
        </>
    )
}

export default InputHandler

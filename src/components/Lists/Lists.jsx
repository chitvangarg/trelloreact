import { useState, useEffect } from "react"
import {
    getAllLists,
    handleListDelete,
    handleListCreation,
} from "../../controllers/listRequests"
import ListCards from "../Cards/ListCards"
import { Flex, Box } from "@chakra-ui/react"
import { useParams } from "react-router-dom"
import ListItem from "./ListItem"
import InputHandler from "../InputHandler"
import { useContext } from "react"
import { backgroundContext } from "../../App"
import Error from "../Error"

const Lists = () => {
    const [lists, setLists] = useState([])
    const [listName, setListName] = useState("")
    const [isLoading, setLoading] = useState(true)
    const [isError, setError] = useState(false)
    const { boardId } = useParams()

    const background = useContext(backgroundContext)

    const createNewList = () => {
        handleListCreation(setLists, setListName, listName, boardId, setError)
    }

    const deleteListItem = (listId) => {
        handleListDelete(listId, setLists, setError)
    }

    useEffect(() => {
        getAllLists(boardId, setLists, setLoading, setError)
        console.log(lists)
    }, [])

    if (isError) {
        return <Error error="Unable to get Lists" />
    } else if (isLoading) {
        return <span className="loader"></span>
    } else {
        return (
            <>
                <Box
                    bgColor={background.bgColor}
                    height="100vh"
                    overflowX="scroll"
                >
                    <Flex
                        columnGap="1vw"
                        py="1rem"
                        justifyContent="space-around"
                        overflow="scroll-x"
                    >
                        {lists.map((item) => {
                            return (
                                <ListItem
                                    list={item}
                                    key={item.id}
                                    handleListDelete={deleteListItem}
                                >
                                    <ListCards listId={item.id} />
                                </ListItem>
                            )
                        })}
                        <InputHandler
                            section="List"
                            inputValue={listName}
                            inputValueHandler={setListName}
                            actionFunction={createNewList}
                        />
                    </Flex>
                </Box>
            </>
        )
    }
}

export default Lists

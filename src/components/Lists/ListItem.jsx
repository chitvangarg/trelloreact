import React from "react"
import { Card, CardHeader, Text, Flex, CardBody } from "@chakra-ui/react"
import { DeleteIcon } from "@chakra-ui/icons"

const ListItem = ({ list, handleListDelete, children }) => {
    const deleteIconStyle = {
        ":hover": {
            cursor: "pointer",
            color: "#ff0000",
        },
    }

    return (
        <>
            <Card bg="#fff" minW="15vw">
                <CardHeader color="#000">
                    <Flex justifyContent="space-between">
                        <Text textTransform="capitalize" fontWeight="bold">{list.name}</Text>
                        <DeleteIcon
                            sx={deleteIconStyle}
                            onClick={() => handleListDelete(list.id)}
                        ></DeleteIcon>
                    </Flex>
                </CardHeader>
                <CardBody>{children}</CardBody>
            </Card>
        </>
    )
}

export default ListItem

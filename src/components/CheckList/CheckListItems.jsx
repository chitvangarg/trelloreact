import React, { useState } from "react"
import { Flex, Box, Text, Button } from "@chakra-ui/react"
import { AddIcon, DeleteIcon } from "@chakra-ui/icons"
import CheckItemsContainer from "../CheckItems/CheckItemsContainer"

const deleteIconStyle = {
    ":hover": {
        cursor: "pointer",
        color: "#ff0000",
    },
}

const CheckListItems = ({
    checkList,
    handleCheckListDeletion,
    updateCheckList
}) => {

    const [checkItemInput, setCheckItemInput] = useState(false)

    return (
        <>
            <Flex
                justifyContent="space-between"
                alignItems="center"
                px="1rem"
                py="0.7rem"
                my="1rem"
                sx={{
                    ":hover": { cursor: "pointer" },
                }}
            >
                <Text>{checkList.name}</Text>

                <Box>
                    <Button onClick={() => setCheckItemInput(true)}>
                        <AddIcon />
                        <Text px="10px" fontSize="0.7rem">
                            Add CheckItems
                        </Text>
                    </Button>

                    <DeleteIcon
                        sx={deleteIconStyle}
                        onClick={() => handleCheckListDeletion(checkList.id)}
                        ml="0.8rem"
                    />
                </Box>
            </Flex>
            <CheckItemsContainer
                checkList={checkList}
                checkItemInput={checkItemInput}
                setCheckItemInput={setCheckItemInput}
                updateCheckList={updateCheckList}
            />
        </>
    )
}

export default CheckListItems

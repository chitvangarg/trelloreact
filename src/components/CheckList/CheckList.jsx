import React, { useEffect, useState } from "react"
import {
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
    Button,
} from "@chakra-ui/react"

import CheckListMapper from "./CheckListMapper"
import InputHandler from "../InputHandler"

import {
    handleChecklistCreation,
    handleCheckListDeletion,
    getAllCheckList,
} from "../../controllers/checkListRequests"
import Error from "../Error"

const CheckList = ({ onClose, isOpen, cardId }) => {
    const [checkLists, setCheckLists] = useState([])
    const [checkListName, setCheckListName] = useState("")
    const [isError, setError] = useState(false)
    const [isLoading, setLoading] = useState(true)

    function createCheckList() {
        handleChecklistCreation(
            checkListName,
            setCheckLists,
            setCheckListName,
            cardId,
            setError
        )
    }

    function deleteCheckList(checkListId) {
        handleCheckListDeletion(checkListId, setCheckLists, setError)
    }

    function updateCheckList(checkListId, result) {
        setCheckLists((checkLists) =>
            checkLists.map((checkList) => {
                return checkList.id === checkListId
                    ? { ...checkList, checkItems: [...result] }
                    : checkList
            })
        )
    }

    useEffect(() => {
        getAllCheckList(cardId, setCheckLists, setError, setLoading)
    }, [])

    if (isLoading) {
        return <span className="loader"></span>
    } else if (isError) {
        return <Error error="Unable to fetch CheckLists" />
    } else {
        return (
            <>
                <Modal isOpen={isOpen} onClose={onClose}>
                    <ModalOverlay />
                    <ModalContent minW="60vw">
                        <ModalHeader>CheckList</ModalHeader>
                        <ModalCloseButton />
                        <ModalBody>
                            <CheckListMapper
                                checkLists={checkLists}
                                deleteCheckList={deleteCheckList}
                                updateCheckList={updateCheckList}
                            />
                            <InputHandler
                                section="CheckList"
                                inputValue={checkListName}
                                inputValueHandler={setCheckListName}
                                actionFunction={createCheckList}
                                id={cardId}
                            />
                        </ModalBody>
                        <ModalFooter>
                            <Button colorScheme="blue" mr={3} onClick={onClose}>
                                Close
                            </Button>
                        </ModalFooter>
                    </ModalContent>
                </Modal>
            </>
        )
    }
}

export default CheckList

import React from "react"
import CheckListItems from "./CheckListItems"
import { Box } from "@chakra-ui/react"

const CheckListMapper = ({checkLists, deleteCheckList, updateCheckList}) => {
    return (
        <>
            {checkLists &&
                checkLists.map((checkList) => {
                    return (
                        <Box
                            w="100%"
                            borderRadius="15px"
                            bgColor="#CAD5E2"
                            boxShadow="rgba(0, 0, 0, 0.02) 0px 1px 3px 0px, rgba(27, 31, 35, 0.15) 0px 0px 0px 1px;"
                            key={checkList.id}
                        >
                            <CheckListItems
                                checkList={checkList}
                                handleCheckListDeletion={deleteCheckList}
                                updateCheckList={updateCheckList}
                            />
                        </Box>
                    )
                })}
        </>
    )
}

export default CheckListMapper

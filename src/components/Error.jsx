import React from 'react'
import ErrorImg from '../assets/error-404.png'
import { Flex, Image, Text } from '@chakra-ui/react'

const Error = ({error}) => {
  return (
    <>
        <Flex minW="100vw" minH="100vh" justifyContent="center" alignItems="center" columnGap="1rem">
            <Image src={ErrorImg} width="70px" height="70px"/>
            <Text fontSize="1.5rem" fontWeight="bolder">{error}</Text>
        </Flex>
    </>
  )
}

export default Error
import React from "react"
import { Button, Box } from "@chakra-ui/react"

const EmptyInputValidation = ({ section, value, displayer, handler, id }) => {
    return (
        <Box
            justifyContent="center"
            alignItems="center"
            w="100%"
            height="100%"
        >
            {value === "" ? (
                <p style={{ color: "#ff0000" }}>
                    Please enter the {section} Name
                </p>
            ) : (
                <Button
                    bg="green"
                    color="#fff"
                    onClick={() => {
                        displayer(false)
                        id !== undefined ? handler(id) : handler()
                    }}
                    my="1rem"
                >
                    Add
                </Button>
            )}
        </Box>
    )
}

export default EmptyInputValidation

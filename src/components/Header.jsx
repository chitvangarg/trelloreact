import React from "react"
import { Button, Heading, Flex, Image } from "@chakra-ui/react"
import { Link, useLocation } from "react-router-dom"
import Logo from "../assets/trello.gif"

const Header = () => {
    const urlPath = useLocation()

    return (
        <>
            <Flex
                justifyContent="space-around"
                bgColor="#0055CC"
                w="100vw"
                alignItems="center"
                py="1rem"
            >
                <Image src={Logo} w="120px" />
                {urlPath.pathname !== "/" &&
                    <Link
                        to="/"
                        style={{ textDecoration: "none", color: "#fff" }}
                    >
                        <Button bgColor="#fff">Boards</Button>
                    </Link>
                }
            </Flex>
        </>
    )
}

export default Header

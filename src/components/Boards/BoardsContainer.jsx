import React from "react"
import { useState, useEffect } from "react"
import { getAllBoards, addBoards } from "../../controllers/boardRequests.js"
import { Flex, Box } from "@chakra-ui/react"
import AddBoard from "./AddBoard"
import BoardCard from "./BoardCard"
import Boards from "./Boards.jsx"

const BoardsContainer = () => {
    const [isError, setError] = useState(false)
    const [isLoading, setLoading] = useState(true)
    const [boardName, setBoardName] = useState("")
    const [isOpen, setOpen] = useState(false)
    const [boards, setBoards] = useState([])

    const onOpen = () => {
        setOpen(true)
    }

    const onClose = () => {
        setOpen(false)
    }

    const addBoardName = (name) => {
        setBoardName(name)
    }

    const addNewBoard = () => {
        addBoards(boardName, setBoards, setBoardName, setOpen, setError)
    }

    useEffect(() => {
        getAllBoards(setBoards, setLoading, setError)
    }, [])

    if (isError) {
        return <Error error="Unable to get boards data" />
    } else if (isLoading) {
        return <span className="loader"></span>
    } else {
        return (
            <>
                <Flex
                    flexWrap="wrap"
                    columnGap="1vw"
                    my="1rem"
                    justifyContent="space-around"
                >
                    <Boards boards={boards} />
                    <Box onClick={onOpen}>
                        <BoardCard
                            boardName={"Create New Board"}
                            background={"#758283"}
                        />
                    </Box>
                    <AddBoard
                        isOpen={isOpen}
                        onClose={onClose}
                        addBoardName={addBoardName}
                        addNewBoard={addNewBoard}
                    />
                </Flex>
            </>
        )
    }
}

export default BoardsContainer

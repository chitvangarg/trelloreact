import React from "react"
import { Card, CardHeader, Heading } from "@chakra-ui/react"

const BoardCard = ({ boardName, background }) => {
    const cardStyles = {
        ":hover": {
            cursor: "pointer",
        },
    }

    return (
        <>
            <Card bgColor={background} w="15vw" h="15vh" sx={cardStyles}>
                <CardHeader textAlign="left">
                    <Heading fontSize="24px" color="#fff">
                        {boardName}
                    </Heading>
                </CardHeader>
            </Card>
        </>
    )
}

export default BoardCard

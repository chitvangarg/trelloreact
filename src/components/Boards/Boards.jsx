import React from "react"
import { Link } from "react-router-dom"
import { useContext } from "react"
import { backgroundContext } from "../../App"
import BoardCard from "./BoardCard"

const Boards = ({ boards }) => {
    const background = useContext(backgroundContext)

    return (
        <>
            {boards.map((board) => {
                return (
                    <Link to={`/board/${board.id}`} key={board.id}>
                        <BoardCard
                            boardName={board.name}
                            background={board.prefs.background}
                            onClick={background.setBgColor(
                                board.prefs.backgroundColor
                                    ? board.prefs.backgroundColor
                                    : board.prefs.backgroundImage
                            )}
                        ></BoardCard>
                    </Link>
                )
            })}
        </>
    )
}

export default Boards

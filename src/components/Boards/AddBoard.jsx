import React from "react"
import {
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
    Input,
    Button,
    InputRightAddon,
    InputGroup,
} from "@chakra-ui/react"

const AddBoard = ({
    isOpen,
    onClose,
    addNewBoard,
    addBoardName,
}) => {
    return (
        <>
            <Modal isOpen={isOpen} onClose={onClose}>
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader>Add New Board</ModalHeader>
                    <ModalCloseButton />
                    <ModalBody>
                        <InputGroup>
                            <Input
                                type="text"
                                w="100%"
                                variant="outline"
                                placeholder="Enter the name of the Board"
                                onChange={(e) => addBoardName(e.target.value)}
                                required
                            ></Input>
                            <InputRightAddon children="X" />
                        </InputGroup>
                    </ModalBody>

                    <ModalFooter>
                        <Button colorScheme="blue" mr={3} onClick={onClose}>
                            Close
                        </Button>
                        <Button variant="ghost" onClick={addNewBoard}>
                            Add Board
                        </Button>
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </>
    )
}

export default AddBoard

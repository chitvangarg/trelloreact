import React from "react"
import {
    Input,
    InputGroup,
    InputRightAddon,
} from "@chakra-ui/react"

const InputNames = ({valueHandler, displayer }) => {
    return (
        <>
            <InputGroup w="inherit" my="1rem">
                <Input
                    type="text"
                    w="250px"
                    variant="outline"
                    placeholder="Enter the name"
                    onChange={(e) => valueHandler(e.target.value)}
                    bgColor="#fff"
                    required
                ></Input>
                <InputRightAddon children="X" onClick={() => displayer(false)} sx={{":hover":{cursor:"pointer"}}}/>
            </InputGroup>
        </>
    )
}

export default InputNames

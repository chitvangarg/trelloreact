import React from "react"
import {Box, Flex} from "@chakra-ui/react"
import { DeleteIcon } from "@chakra-ui/icons"

const Card = ({handleCardDelete, card, onOpen}) => {

    const deleteIconStyle = {
        ":hover": {
            cursor: "pointer",
            color: "#ff0000",
        },
    }

    return (
        <>
            <Flex
                bg="#EAF0F1"
                px="0.7rem"
                py="0.8rem"
                my="0.8rem"
                borderRadius="12px"
                justifyContent="space-between"
                sx={{ ":hover": { cursor: "pointer" } }}
                key={card.id}
            >
                <Box
                    onClick={() => {
                        onOpen()
                    }}
                    w="100%"
                >
                    {card.name}
                </Box>
                <DeleteIcon
                    sx={deleteIconStyle}
                    onClick={() => handleCardDelete(card.id)}
                />
            </Flex>
        </>
    )
}

export default Card

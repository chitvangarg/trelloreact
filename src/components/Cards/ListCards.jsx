import React, { useEffect, useState } from "react"
import CheckList from "../CheckList/CheckList"
import {
    getAllCards,
    handleCardDelete,
    handleCardInsertion,
} from "../../controllers/cardRequests"
import Card from "./Card"
import InputHandler from "../InputHandler"
import Error from "../Error"

const ListCards = ({ listId }) => {
    const [isOpen, setOpen] = useState(false)
    const [isLoading, setLoading] = useState(false)
    const [isError, setError] = useState(false)
    const [cards, setCards] = useState([])
    const [cardName, setCardName] = useState("")

    function deleteCard(cardId) {
        handleCardDelete(cardId, setCards, setError)
    }

    function createCard() {
        handleCardInsertion(listId, cardName, setCardName, setCards, setError)
    }

    useEffect(() => {
        getAllCards(listId, setCards, setLoading, setError)
    }, [])

    function onOpen() {
        setOpen(true)
    }

    function onClose() {
        setOpen(false)
    }

    if (isError) {
        return <Error error="Unable to get Cards" />
    } else if (isLoading) {
        return <span className="loader"></span>
    } else {
        return (
            <>
                {cards &&
                    cards.map((card) => {
                        return (
                            <>
                                <Card
                                    handleCardDelete={deleteCard}
                                    card={card}
                                    onOpen={onOpen}
                                />
                                <CheckList
                                    isOpen={isOpen}
                                    onClose={onClose}
                                    cardId={card.id}
                                />
                            </>
                        )
                    })}
                <InputHandler
                    section="Card"
                    inputValue={cardName}
                    inputValueHandler={setCardName}
                    actionFunction={createCard}
                />
            </>
        )
    }
}

export default ListCards

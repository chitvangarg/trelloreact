import axios from "axios"

const apiKey = "f1ff9ddb52d1f49907bf96e30efa560f"
const apiToken =
    "ATTA296eee4fea190c8f20a57242675b02fc462c4b75cffcd52f4537c951e8e6281bD2F261AA"
const baseURL = "https://api.trello.com/1/"

// BOARDS REQUESTS

// Get borads data

const getBoards = async () => {
    try {
        let boards = await axios.get(
            `${baseURL}members/me/boards?key=${apiKey}&token=${apiToken}`
        )

        if (boards.status === 200) {
            return boards.data
        } else {
            throw new Error("Unable to fetch data")
        }
    } catch (err) {
        throw new Error(err.message)
    }
}

// Create new Board

const addNewBoard = async (boardName) => {
    try {
        const res = await axios.post(
            `${baseURL}boards/?name=${boardName}&key=${apiKey}&token=${apiToken}`
        )

        if (res.status === 200) {
            return res.data
        } else {
            throw new Error("Unable to fetch data")
        }
    } catch (err) {
        throw new Error(err.message)
    }
}

// LISTS REQUESTS

// add list

const addNewList = async (listName, boardId) => {
    try {
        const list = await axios.post(
            `${baseURL}lists?name=${listName}&idBoard=${boardId}&key=${apiKey}&token=${apiToken}`
        )

        if (list.status === 200) {
            return list.data
        } else {
            throw new Error("Unable to fetch data")
        }
    } catch (err) {
        throw new Error(err.message)
    }
}

// get lists

const getLists = async (boardId) => {
    try {
        let lists = await axios.get(
            `${baseURL}boards/${boardId}/lists?key=${apiKey}&token=${apiToken}`
        )

        if (lists.status === 200) {
            return lists.data
        } else {
            throw new Error("Unable to fetch data")
        }
    } catch (err) {
        throw new Error(err.message)
    }
}

// delete list

const deleteList = async (listId) => {
    try {
        const res = await axios.put(
            `${baseURL}lists/${listId}/closed?key=${apiKey}&token=${apiToken}&value=true`
        )
        if (res.status === 200) {
            return res.data
        } else {
            throw new Error("Unable to fetch data")
        }
    } catch (err) {
        throw new Error(err.message)
    }
}

// CARD API

const createCard = async (listId, cardName) => {
    try {
        const card = await axios.post(
            `${baseURL}cards?idList=${listId}&key=${apiKey}&token=${apiToken}&name=${cardName}`
        )

        if (card.status === 200) {
            return card.data
        } else {
            throw new Error("Unable to fetch data")
        }
    } catch (err) {
        throw new Error(err.message)
    }
}

// Get Card

const getCards = async (listId) => {
    try {
        const cards = await axios.get(
            `${baseURL}lists/${listId}/cards?key=${apiKey}&token=${apiToken}`
        )

        if (cards) {
            return cards.data
        } else {
            throw new Error("Unable to fetch data")
        }
    } catch (err) {
        throw new Error(err.message)
    }
}

// Delete Card

const deleteCard = async (cardId) => {
    try {
        const res = await axios.delete(
            `${baseURL}cards/${cardId}?key=${apiKey}&token=${apiToken}`
        )

        if (res.status === 200) {
            return res.data
        } else {
            throw new Error("Unable to delete card")
        }
    } catch (err) {
        throw new Error(err.message)
    }
}

// CHECKLIST

// create checklist

const createCheckList = async (cardId, checkListName) => {
    try {
        const checklist = await axios.post(
            `${baseURL}checklists?idCard=${cardId}&key=${apiKey}&token=${apiToken}&name=${checkListName}`
        )

        if (checklist.status === 200) {
            return checklist.data
        } else {
            throw new Error("Unable to create checklist")
        }
    } catch (err) {
        throw new Error(err.message)
    }
}

const getCheckList = async (cardId) => {
    try {
        const checklists = await axios.get(
            `${baseURL}cards/${cardId}/checklists?key=${apiKey}&token=${apiToken}`
        )

        if (checklists) {
            return checklists.data
        } else {
            throw new Error("No checklist found")
        }
    } catch (error) {
        throw new Error(err.message)
    }
}

const deleteCheckList = async (checkListID) => {
    try {
        const res = await axios.delete(
            `${baseURL}checklists/${checkListID}?key=${apiKey}&token=${apiToken}`
        )

        if (res.status === 200) {
            return res.data
        } else {
            throw new Error("Unable to delete checklist")
        }
    } catch (err) {
        throw new Error(err.message)
    }
}

// CHECKITEMS REQUEST

// create checkitem

const createCheckItem = async (checkListID, checkItemName) => {
    try {
        const checkItem = await axios.post(
            `${baseURL}checklists/${checkListID}/checkItems?name=${checkItemName}&key=${apiKey}&token=${apiToken}`
        )

        if (checkItem.status === 200) {
            return checkItem.data
        } else {
            throw new Error("Unable to create checkItem")
        }
    } catch (err) {
        throw new Error(err.message)
    }
}

const getCheckItems = async (checkListID) => {
    try {
        const checkItems = await axios.get(
            `${baseURL}checklists/${checkListID}/checkItems?key=${apiKey}&token=${apiToken}`
        )

        if (checkItems.status === 200) {
            return checkItems.data
        } else {
            throw new Error("No checkItem found")
        }
    } catch (error) {
        throw new Error(error.message)
    }
}

const deleteCheckItem = async (checkListID, checkItemId) => {
    try {
        const res = await axios.delete(
            `${baseURL}checklists/${checkListID}/checkItems/${checkItemId}?key=${apiKey}&token=${apiToken}`
        )

        if (res.status === 200) {
            return res.data
        } else {
            throw new Error("Unable to delete Checkitem")
        }
    } catch (err) {
        throw new Error(err.message)
    }
}

// Update CheckItems
const updateCheckItem = async (cardId, checkItemId, status) => {
    try {
        const state = {
            state: status,
        }

        const res = await axios.put(
            `${baseURL}cards/${cardId}/checkItem/${checkItemId}?key=${apiKey}&token=${apiToken}`,
            state
        )

        if (res.status === 200) {
            return res.data
        } else {
            throw new Error("Unable to delete Checkitem")
        }
    } catch (err) {
        throw new Error(err.message)
    }
}
export {
    getBoards,
    addNewBoard,
    addNewList,
    getLists,
    deleteList,
    createCard,
    getCards,
    deleteCard,
    createCheckList,
    getCheckList,
    deleteCheckList,
    createCheckItem,
    getCheckItems,
    deleteCheckItem,
    updateCheckItem,
}

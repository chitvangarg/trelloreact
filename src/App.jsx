import "./App.css"
import BoardsContainer from "./components/Boards/BoardsContainer"
import Header from "./components/Header"
import { Routes, Route } from "react-router-dom"
import Lists from "./components/Lists/Lists"
import { createContext, useState } from "react"

export const backgroundContext = createContext()

function App() {
    const [bgColor, setBgColor] = useState("#0079BF")

    return (
        <backgroundContext.Provider value={{ bgColor, setBgColor }}>
            <Header />
            <Routes>
                <Route path="/" element={<BoardsContainer />} />
                <Route path="/board/:boardId" element={<Lists />}></Route>
            </Routes>
        </backgroundContext.Provider>
    )
}

export default App

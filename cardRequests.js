import { createCard, deleteCard, getCards } from "./apiRequests"

export const handleCardInsertion = (
    listId,
    cardName,
    setCardName,
    setCards,
    setError
) => {
    if (cardName !== "") {
        createCard(listId, cardName)
            .then((card) => {
                setCards((prevCards) => [...prevCards, card])
                setCardName("")
            })
            .catch((err) => {
                setError(true)
            })
    }
}

export const handleCardDelete = (cardId, setCards, setError) => {
    deleteCard(cardId)
        .then((resultingCard) => {
            console.log(resultingCard)
            setCards((cards) => cards.filter((card) => card.id !== cardId))
        })
        .catch((err) => setError(true))
}

export const getAllCards = (listId, setCards, setLoading, setError) => {
    getCards(listId)
        .then((listCards) => {
            setCards([...listCards])
            setLoading(false)
        })
        .catch((err) => setError(true))
}
